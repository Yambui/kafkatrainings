package kafkatutor;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class ProducerCallbackKeysDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        final Logger logger = LoggerFactory.getLogger(ProducerCallbackKeysDemo.class);
        //Create producer props
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        //Create producer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);
        //Create ProducerRecord
        for (int i=0; i<10; i++) {

            String topic = "anton_topic";
            String value = "Here we go again" + i;
            String key = "id_" + i;
            ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>(topic, key, value);
            logger.info("Key: " + key);
            //id 1 goes to 0 partition

            //Do something
            producer.send(producerRecord, new Callback() {
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    //Executes when sent or exeption
                    if (e == null) {
                        logger.info("new metadata recieved \n" + recordMetadata.topic()
                                + "\n" + recordMetadata.partition()
                                + " \n" + recordMetadata.offset()
                                + "\n" + recordMetadata.timestamp());
                    } else {
                        logger.error("NOT SENT");
                    }
                }
                //makes all stuf synchronous - bad
            }).get();
        }
        //Send
        producer.flush();
        //Close
        producer.close();
    }
}
