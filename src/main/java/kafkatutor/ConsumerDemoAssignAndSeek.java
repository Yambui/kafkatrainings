package kafkatutor;


import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

public class ConsumerDemoAssignAndSeek {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(ConsumerDemoAssignAndSeek.class);
        String bootstrapServer = "127.0.0.1:9092";
        String groupId = "my-application-mer";
        String topic = "anton_topic";
        //configs
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        //create consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);

        long offsetToReadFrom = 15L;
        //assign and seek are mostly used to replay data or fetch a specific message
        TopicPartition topicPartitionToRead = new TopicPartition(topic, 0);
        consumer.assign(Arrays.asList(topicPartitionToRead));
        //seek
        consumer.seek(topicPartitionToRead, offsetToReadFrom);

        boolean keepReading = true;
        int numberOfMessRedSoFar = 0;
        //pool for new data
        while (keepReading) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100l));
            for (ConsumerRecord<String, String> record : records) {
                numberOfMessRedSoFar += 1;
                logger.info("Key - " + record.key() + "\n Value - " + record.value() + "\n Partition - " + record.partition() + "\n Offset - " + record.offset());
                if (numberOfMessRedSoFar >= 5) {
                    keepReading = false;
                    break;
                }
            }
        }
        logger.info("Stoping application");
    }
}

