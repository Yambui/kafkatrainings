package kafkatutor;


import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class ConsumerDemoGroupThreads {
    public static void main(String[] args) {

        new ConsumerDemoGroupThreads().run();

    }

    public void run() {
        Logger logger = LoggerFactory.getLogger(ConsumerDemoGroupThreads.class);
        String topic = "anton_topic";
        logger.info("Creating consumer thread!");
        CountDownLatch latch = new CountDownLatch(1);
        Runnable myConsumer = new ConsumerThread(latch, topic);
        Thread myThread = new Thread(myConsumer);
        myThread.start();
        Runtime.getRuntime().addShutdownHook(new Thread(
                () -> {logger.info("got shutdown hook");
                    ((ConsumerThread)myConsumer).shutdown();
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    logger.info("operation exited");
                }
        ));
        try {
            latch.await();
        } catch (InterruptedException e) {
            logger.error("Application interrupted");
        } finally {
            logger.info("Application terminated is closing");
        }
    }

    public class ConsumerThread implements Runnable {

        private CountDownLatch latch;
        private KafkaConsumer<String, String> consumer;
        private Logger logger = LoggerFactory.getLogger(ConsumerThread.class);

        public ConsumerThread (CountDownLatch latch, String topic) {
            //configs
            String bootstrapServer = "127.0.0.1:9092";
            String groupId = "my-application-mer";
            Properties properties = new Properties();
            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
            //create consumer
            consumer = new KafkaConsumer<String, String>(properties);
            //subscribe to topic
            consumer.subscribe(Collections.singleton(topic));
            //pool for new data
            this.latch = latch;
        }

        @Override
        public void run() {
            try {
            while(true){
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100l));
                for(ConsumerRecord<String, String> record: records) {
                    logger.info("Key - "+ record.key() +"\n Value - "+ record.value() + "\n Partition - " + record.partition() + "\n Offset - "+ record.offset());
                }
            }
            } catch (WakeupException ex) {
                logger.info("Recieved signal to shut down!");
            } finally {
                consumer.close();
                //tell code to stop
                latch.countDown();
            }
        }

        public void shutdown(){
            consumer.wakeup();
        }
    }
}
