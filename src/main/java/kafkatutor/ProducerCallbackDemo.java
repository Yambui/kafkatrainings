package kafkatutor;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerCallbackDemo {
    public static void main(String[] args) {

        final Logger logger = LoggerFactory.getLogger(ProducerCallbackDemo.class);
        //Create producer props
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        //Create producer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);
        //Create ProducerRecord
        for (int i=0; i<10; i++) {
            ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>("anton_topic", "Well here we come" + i);
            //Do something
            producer.send(producerRecord, new Callback() {
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    //Executes when sent or exeption
                    if (e == null) {
                        logger.info("new metadata recieved \n" + recordMetadata.topic()
                                + "\n" + recordMetadata.partition()
                                + " \n" + recordMetadata.offset()
                                + "\n" + recordMetadata.timestamp());
                    } else {
                        logger.error("NOT SENT");
                    }
                }
            });
        }
        //Send
        producer.flush();
        //Close
        producer.close();
    }
}
